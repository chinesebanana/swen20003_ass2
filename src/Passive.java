/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.SlickException;
import java.util.Random;

/** Represents all the Passive Monsters.
 */
public class Passive extends Monster {

	// Constants
	private static final double SPEED = 0.2;
	private static final String IMGPATH = RPG.UNITS_PATH+"dreadbat.png";
	private static final int MAX_DAMAGE = 0;
	private static final int COOLDOWN = 0;
	
	// For wandering and running away
	private static final int TIMETORUN = 5000;
	private static final int TIMETOWANDER = 3000;
	private boolean isAttacked = false;
	private double runawayTimer;
	private double wanderTimer;
	private static final int[] DIRECTIONS = {-1, 0, 1};
	private int directionX;
	private int directionY;
	
	/** Creates a new Passive Monster.
	 * @param name The name of the Monster.
     * @param x The Monster's starting x location in pixels.
	 * @param y The Monster's starting y location in pixels.
	 * @param hp The Monster's starting HP value.
     * @throws SlickException Library-specific exception.
     */
	public Passive(String name, int x, int y, int hp)
			throws SlickException {
		super(name, IMGPATH, x, y, hp, MAX_DAMAGE, COOLDOWN);
		
		// Initialise random wandering
		runawayTimer = -1;
		wanderTimer = TIMETOWANDER;
		directionX = randomDirection(DIRECTIONS);
		directionY = randomDirection(DIRECTIONS);
	}
	
	/** Gets a random number from -1, 0, 1
	 * @param directions List of integers representing the directions
	 * @return a random number from directions
	 */
	public static int randomDirection(int[] directions) {
	    int i = new Random().nextInt(directions.length);
	    return directions[i];
	}
	
	/** Moves Monster in random directions.
	 * @param world Passed in as an object to check for terrain-blocking
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void wander(World world, double delta) {
		double newX, newY;
		newX = getX() + (directionX * SPEED * delta);
		newY = getY() + (directionY * SPEED * delta);
		
		if (!world.terrainBlocks(newX, newY)) {
			setX(newX);
			setY(newY);
		}
		wanderTimer -= delta;
	}
	
	/** When getting attacked, set underAttack to true.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void isAttacked(double delta) {
		runawayTimer = TIMETORUN;
		isAttacked = true;
	}
	
	/** Get new Y for in regard to Player
	 * @param distTotal Euclid distance between Player and Monster.
	 * @param playerY Y coordinates for Player.
	 * @param delta Time passed since last frame (milliseconds).
	 * @return new y coordinate for Monster.
	 */
	public double getNewY(double distTotal, double playerY, double delta) {
		double distY = ((this.getY() - playerY)/distTotal) * SPEED * delta;
		return getY() + distY;
	}
	
	/** Get new X for in regard to Player
	 * @param distTotal Euclid distance between Player and Monster.
	 * @param playerX X coordinates for Player.
	 * @param delta Time passed since last frame (milliseconds).
	 * @return new x coordinate for Monster.
	 */
	public double getNewX(double distTotal, double playerX, double delta) {
		double distX = ((this.getX() - playerX)/distTotal) * SPEED * delta;
		return getX() + distX;
	}

	/** Moves the Monster when Player comes too close.
	 * @param world Passed in as an object to check for terrain-blocking
	 * @param playerX The x coordinates of Player on map.
	 * @param playerY The y coordinates of Player on map.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	private void runAway(World world, double playerX, double playerY, double delta) {
		
		double distTotal = distance(playerX, playerY);
		double newX, newY;		

		newX = getNewX(distTotal, playerX, delta);
		newY = getNewY(distTotal, playerY, delta);
		
		if (!world.terrainBlocks(newX, newY)) {
			setX(newX);
			setY(newY);
		}
		
		runawayTimer -= delta;
	}
	
	/** Moves the Monster in the correct motion according to the situation.
	 * @param world Passed in as an object to check for terrain-blocking
	 * @param playerX The x coordinates of Player on map.
	 * @param playerY The y coordinates of Player on map.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(World world, double playerX, double playerY, double delta) {
		if (isDead())
			return;
		
		// use this to call everything else, ie. runaway until timer < 1000. 		
		if (runawayTimer > 0 && isAttacked) {
			runAway(world, playerX, playerY, delta);
		} else {
			
			isAttacked = false; //Reset attack status
			
			/* Roam freely again */
			if (wanderTimer > 0) {
				wander(world, delta);
			} else {
				// Reset random directions
				wanderTimer = TIMETOWANDER;
				directionX = randomDirection(DIRECTIONS);
				directionY = randomDirection(DIRECTIONS);
				wander(world, delta);
			}
		}
	}

}
