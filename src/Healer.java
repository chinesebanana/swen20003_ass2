/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/** Represents villager, Healer.
 */
public class Healer extends Villager {
	
	// Constants
	private static final int STARTX = 738;
	private static final int STARTY = 549;
	private static final int STARTX_2 = 6200;
	private static final int STARTY_2 = 6700;
	private static final String NAME = "Elvira";
	private static final String NAME_2 = "Alvina";
	private static final String IMG = "/units/shaman.png";
	private static final String SOUND = "/sounds/heal.ogg";
	
	// Dialogues
	private static final String DIALOGUE_1 = "Return to me if you ever need healing.";
	private static final String DIALOGUE_2 = "You're looking much healthier now.";
	
	private Sound heal;
	
	
	/** Create new Healer object (Elvira).
	 * 
	 * @throws SlickException Library-specific exception.
	 */
	public Healer() throws SlickException {
		super(NAME, RPG.ASSETS_PATH + IMG, STARTX, STARTY);
		heal = new Sound(RPG.ASSETS_PATH + SOUND);
	}
	
	/** Create new Healer object. (Alvina)
	 * @param imgpath Path of Unit's image file.
	 * @throws SlickException Library-specific exception.
	 */
	public Healer(String imgpath) throws SlickException {
		super(NAME_2, imgpath, STARTX_2, STARTY_2);
		heal = new Sound(RPG.ASSETS_PATH + SOUND);
	}
	
	/** Heals Player
	 * @param player Player passed in as an object.
	 */
	public void action(Player player) {
		heal.play();
		player.updateHealth(-1); // Brings player's hp to MaxHp.
	}

	@Override
	public String dialogue(Player player) {
		startDialogueTimer();
		
		if (player.getHealthPerc() == 1.0)
			return DIALOGUE_1;
		
		action(player);
		return DIALOGUE_2;
	}
}
