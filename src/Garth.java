/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern Lee <joannal1>
 */

import org.newdawn.slick.SlickException;

/** Represents the villager, Garth.
 */
public class Garth extends Villager {
	
	// Initialising Constants
	private static final int STARTX = 756;
	private static final int STARTY = 870;
	private static final String NAME = "Garth";
	private static final String IMG = "/units/peasant.png";
	
	// Dialogues
	private static final String DIALOGUE_1 = "Find the Amulet of Vitality, across the river to the west.";
	private static final String DIALOGUE_2 = "Find the Sword of Strength - cross the bridge to the east, then head south.";
	private static final String DIALOGUE_3 = "Find the Tome of Agility, in the Land of Shadows.";
	private static final String DIALOGUE_4 = "You have found all the treasure I know of.";
	
	// Item names
	private static final String ITEM_1 = "amulet";
	private static final String ITEM_2 = "sword";
	private static final String ITEM_3 = "tome";
	
	private static Garth _instance = null;
	
	/** Creates new Garth singleton object.
	 * 
	 * @throws SlickException Library-specific exception.
	 */
	protected Garth() throws SlickException {
		super(NAME, RPG.ASSETS_PATH + IMG, STARTX, STARTY);
	}
	
	public static Garth getInstance() throws SlickException {
        if (_instance == null)
            _instance = new Garth();
        return _instance;
	}

	@Override
	public String dialogue(Player player) {
		startDialogueTimer();
		
		Item[] inventory = player.getInventory();
		boolean amulet = false, sword = false, tome = false;
		
		for (int i=0; i < inventory.length; i++) {
			if (inventory[i] != null) {
				if (inventory[i].getName().equals(ITEM_1))
					amulet = true;
				else if (inventory[i].getName().equals(ITEM_2))
					sword = true;
				else if (inventory[i].getName().equals(ITEM_3))
					tome = true;
			}
		}
		
		if (!amulet)
			return DIALOGUE_1;
		if (!sword)
			return DIALOGUE_2;
		if (!tome)
			return DIALOGUE_3;
		
		return DIALOGUE_4;
	}

}
