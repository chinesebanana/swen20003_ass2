/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/** Super class for all Monsters
 */
public class Monster extends Unit {
	
	private Boolean isDead = false;
	
    /** Creates a new Monster.
     * @param name Name of Monster.
     * @param imgPath Path of Monster's image file.
     * @param x The Monster's starting x location in pixels.
     * @param y The Monster's starting y location in pixels.
     * @param hp The Monster's starting HP value.
     * @param maxDamage The maximum damage the Monster can do.
     * @param cooldown The minimum length of time the Monster has to wait between attacks, in milliseconds.
     * @throws SlickException Library-specific exception.
     */
	public Monster(String name, String imgPath, double x, double y, int hp, int maxDamage, int cooldown)
		throws SlickException
	{
		super(name, imgPath, x, y, hp, maxDamage, cooldown);
	}
	
	/** Checks if Monster is dead
	 * @return true if Monster is dead.
	 */
	public Boolean isDead() {
		return isDead;
	}
	
	/** Calculates Euclidean distance between Monster and Player.
	 * @param playerX The Player's x coordinates in pixels.
	 * @param playerY The Player's y coordinates in pixels.
	 * @return Euclidean distance between Monster and Player.
	 */
	protected double distance(double playerX, double playerY) {
		double distX, distY;
		double distTotal;
		distX = playerX - this.getX();
		distY = playerY - this.getY();
		distTotal = Math.sqrt(Math.pow(distX,2) + Math.pow(distY,2));
		return distTotal;
	}

	/** Updates the HP of the Monster, checks if the damage is big enough to kill Monster 
	 * @param damage The damage sustained by the Monster.
	 */
	public void updateHealth(int damage) {
		if (damage >= getHp()) {
			isDead = true;
		} else {
			setHp(getHp() - damage);
		}
	}
	
	/** Draw the Monster to the screen at the correct place.
	 */
	public void render() {
		if (!isDead)
			getImg().drawCentered((int) getX(), (int) getY());
	}
	
	/** Renders the Monster's health panel.
	 * @param g The current Slick graphics context.
	 */
	public void renderPanel(Graphics g) {
		if (!isDead) {
			// Colors used
	        Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f);   // Black, transp
	        Color BAR = new Color(0.8f, 0.0f, 0.0f, 0.8f);      // Red, transp
	        Color NAME = new Color(1.0f, 1.0f, 1.0f);          // White
	        
			// Render health bar
	        int bar_width = g.getFont().getWidth(getName()) + 20;
	        int bar_height = 18;
	        int bar_x = (int) getX() - bar_width / 2;
	        int bar_y = (int) getY() - 3 * bar_height;
	        float health_percent = getHp()/getMaxHp();
	        int hp_bar_width = (int) (bar_width * health_percent);
	        g.setColor(BAR_BG);
	        g.fillRect(bar_x, bar_y, bar_width, bar_height);
	        g.setColor(BAR);
	        g.fillRect(bar_x, bar_y, hp_bar_width, bar_height);
	        
	        int text_x = bar_x + (bar_width - g.getFont().getWidth(getName())) / 2;
	        int text_y = bar_y + (bar_height - g.getFont().getHeight(getName())) / 2;
	        g.setColor(NAME);
	        g.drawString(getName(), text_x, text_y);
		}
	}
}
