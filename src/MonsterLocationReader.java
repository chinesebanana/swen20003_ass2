/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern Lee <joannal1>
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/** This class is used to read the locations of the Monsters from the provided .dat file.
 */
public class MonsterLocationReader {
	
	private static final int NAMES = 0;
	private static final int X = 1;
	private static final int Y = 2;
	private static final int OFFSET = 4;
	private static final int NUM_OF_VARIABLES = 3;
	private static final int NUM_OF_MONSTERS = 127;
	
	/** Loads the locations as a 2D array of Strings from a file.
	 * @param filename Location of the file to work on.
	 * @return 2d array of strings, representing names and locations of Monsters.
	 */
	public static String[][] loadLocations(String filename) {

        BufferedReader br = null;
        String line = "";
        String splitBy = ",";
        
        String tuples[][] = new String[NUM_OF_VARIABLES][NUM_OF_MONSTERS];
        
        try {
            br = new BufferedReader(new FileReader(filename));
            
            int i = 0;
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] location = line.split(splitBy);
                
                if (i >= OFFSET) {
                	tuples[NAMES][i - OFFSET] = location[NAMES]; // names
                	tuples[X][i - OFFSET] = location[X]; // x coordinates
                	tuples[Y][i - OFFSET] = location[Y]; // y coordinates
                }
                i += 1;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return tuples;
    }
}
