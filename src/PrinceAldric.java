import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/** Represents the Villager, Prince Aldric.
 */
public class PrinceAldric extends Villager {
	// Initialising Constants
	private static final int STARTX = 467;
	private static final int STARTY = 679;
	private static final String NAME = "Prince Aldric";
	private static final String IMG = "/units/prince.png";
	private static final String ITEM = "elixir";
	private static final String SOUND = "/sounds/end_game.ogg";
	
	// Dialogues
	private static final String DIALOGUE_1 = "The elixir! My father is cured! Thank you!";
	private static final String DIALOGUE_2 = "Please seek out the Elixir of Life to cure the king.";
	
	private Sound endgame;
	
	private static PrinceAldric _instance = null;
	
	/** Creates a new PrinceAldric singleton object.
	 * 
	 * @throws SlickException Library-specific exception.
	 */
	protected PrinceAldric() throws SlickException {
		super(NAME, RPG.ASSETS_PATH + IMG, STARTX, STARTY);
		
		endgame = new Sound(RPG.ASSETS_PATH + SOUND);
	}
	
	public static PrinceAldric getInstance() throws SlickException {
        if (_instance == null)
            _instance = new PrinceAldric();
        return _instance;
	}

	@Override
	public String dialogue(Player player) {
		startDialogueTimer();
		
		Item[] inventory = player.getInventory();
		
		for (int i=0; i < inventory.length; i++) {
			if (inventory[i] != null && inventory[i].getName().equals(ITEM)) {
				action(player);
				return DIALOGUE_1;
			}
		}
		return DIALOGUE_2;
	}
	
	/** Check if Player already has the Elixir, if yes then end game
	 * @param player Player to check.
	 */
	public void action(Player player) {
		endgame.play();
		player.setEnd();
	}

}
