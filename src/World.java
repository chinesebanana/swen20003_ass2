/* 433-294 Object Oriented Software Development
 * RPG Game Engine
 * Sample Solution
 * Author: Matt Giuca <mgiuca>
 */

import org.lwjgl.openal.AL;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.Font;

/** Represents the entire game world.
 * (Designed to be instantiated just once for the whole game).
 */
public class World
{
	// File locations 
	private static final String MAP_FILENAME = RPG.ASSETS_PATH + "/map.tmx";
	private static final String PLAYER_FILENAME = RPG.UNITS_PATH + "/player.png";
	private static final String PEASANT_FILENAME = RPG.UNITS_PATH + "/shaman2.png";
	private static final String MONSTERS_FILENAME = "data/units.dat";
	private static final String IMAGE_EXTENSION = ".png";
	
	// Starting locations for Player and Items
    public static final int PLAYER_START_X = 756, PLAYER_START_Y = 684;
    private static final int AMULET_X = 965, AMULET_Y = 3563;
    private static final int SWORD_X = 546, SWORD_Y = 6707;
    private static final int TOME_X = 4791, TOME_Y = 1253;
    private static final int ELIXIR_X = 1976, ELIXIR_Y = 402;
    
    private static final int NUM_OF_PASSIVE_MONSTERS = 30;
    private static final int NUM_OF_AGGRESSIVE_MONSTERS = 97;
    private static final int NUM_OF_VILLAGERS = 4;
    private static final String[] MONSTERS = {"GiantBat", "Zombie", "Bandit", "Skeleton", "Draelic"};
    private static final int[] HP = {40, 60, 40, 100, 140};
    private static final int[] MAX_DAMAGE = {0, 10, 8, 16, 30};
    private static final int[] COOLDOWN = {0, 800, 200, 500, 400};

    // Constants for Items
    private static final int NUM_OF_ITEMS = 4;
    
    
    private TiledMap map = null;
    private Camera camera = null;
    private Font nicefont = null;
    
    // Objects to appear in World
    private Player player = null;
    private Item[] items;
    private Villager[] villagers;
    private Passive[] passiveMonsters;
    private Aggressive[] aggressiveMonsters;

    /** Map width, in pixels. */
    private int getMapWidth()
    {
        return map.getWidth() * getTileWidth();
    }

    /** Map height, in pixels. */
    private int getMapHeight()
    {
        return map.getHeight() * getTileHeight();
    }

    /** Tile width, in pixels. */
    private int getTileWidth()
    {
        return map.getTileWidth();
    }

    /** Tile height, in pixels. */
    private int getTileHeight()
    {
        return map.getTileHeight();
    }

    /** Create a new World object. */
    public World()
    throws SlickException
    {
        map = new TiledMap(MAP_FILENAME, RPG.ASSETS_PATH);
        player = new Player(PLAYER_FILENAME, PLAYER_START_X, PLAYER_START_Y);
        camera = new Camera(player, RPG.SCREEN_WIDTH, RPG.SCREEN_HEIGHT);
        
        nicefont = FontLoader.loadFont(RPG.ASSETS_PATH + "/DejaVuSans-Bold.ttf", 13);
        
        // Initialising all items
        items = new Item[NUM_OF_ITEMS];
        items[0] = new Item("amulet", RPG.ITEMS_PATH + "amulet" + IMAGE_EXTENSION, AMULET_X, AMULET_Y);
        items[1] = new Item("sword", RPG.ITEMS_PATH + "sword" + IMAGE_EXTENSION, SWORD_X, SWORD_Y);
        items[2] = new Item("tome", RPG.ITEMS_PATH + "tome" + IMAGE_EXTENSION, TOME_X, TOME_Y);
        items[3] = new Item("elixir", RPG.ITEMS_PATH + "elixir" + IMAGE_EXTENSION, ELIXIR_X, ELIXIR_Y);
        
        // Initialising all villagers
        villagers = new Villager[NUM_OF_VILLAGERS];
        villagers[0] = PrinceAldric.getInstance();
        villagers[1] = new Healer();
        villagers[2] = Garth.getInstance();
        villagers[3] = new Healer(PEASANT_FILENAME);
        
        // Initialising Monsters
        passiveMonsters = new Passive[NUM_OF_PASSIVE_MONSTERS];
        aggressiveMonsters = new Aggressive[NUM_OF_AGGRESSIVE_MONSTERS];
        
        String tuples[][] = MonsterLocationReader.loadLocations(MONSTERS_FILENAME);
        int names = 0;
        int x = 1;
        int y = 2;
        
        int posX, posY;
        int monNameIndex = 1;
        String imgpath;
        
        for (int i = 0; i < tuples[0].length; i++) {
        	posX = Integer.parseInt(tuples[x][i]);
    		posY = Integer.parseInt(tuples[y][i]);

        	if (tuples[names][i].equals(MONSTERS[0])) { // Passive Monsters
        		passiveMonsters[i] = new Passive(tuples[names][i],posX,posY,HP[0]);
        	} else { // Aggressive Monsters
        		imgpath = RPG.UNITS_PATH + tuples[names][i] + IMAGE_EXTENSION;
        		
        		if (!MONSTERS[monNameIndex].equals(tuples[names][i]))
        			monNameIndex += 1;
        		
        		aggressiveMonsters[i - NUM_OF_PASSIVE_MONSTERS] = new Aggressive(tuples[names][i],
            		imgpath, posX, posY, HP[monNameIndex], MAX_DAMAGE[monNameIndex], COOLDOWN[monNameIndex]);        			
        	}
        }
    }

    /** Update the game state for a frame.
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @param attack Check if any attacks occuring.
     * @param talk Check if any talks occuring.
     * @throws SlickException Library-specific exception.
     */
    public void update(int dir_x, int dir_y, int delta, boolean attack, boolean talk)
    throws SlickException
    {    	
    	double distX;
    	double distY;
    	
    	// Player is attacking monsters
        if (attack)
        	combat(delta);
        
        // Player is interacting with villagers
        if (talk) {
        	for (int i = 0; i < villagers.length; i++) {
        		distX = Math.abs(villagers[i].getX()-player.getX());
            	distY = Math.abs(villagers[i].getY()-player.getY());
            	
            	if (distX < 50 && distY < 50)
            		villagers[i].setDialogue(villagers[i].dialogue(player));
            }
        }
        
        for (int i = 0; i < villagers.length; i++) {
        		villagers[i].setDialogueTimer(delta);
        }
        
        if (player.getEnd()) {
        	player.updateEndTimer(delta);
        	if (player.getEndTimer() < 0) { // END GAME
            	AL.destroy();
            	System.exit(0);
        	}
        }
        
        player.updateAttackTimer(delta);
    	player.move(this, dir_x, dir_y, delta);
        camera.update();
        
        // Check if player is near any Passive Monsters
        for (int i = 0; i < passiveMonsters.length; i++) {
        	passiveMonsters[i].update(this, player.getX(), player.getY(), delta);
        }
    	
    	// Check if player is near any Aggressive Monsters
        for (int i = 0; i < aggressiveMonsters.length; i++) {
        	distX = Math.abs(aggressiveMonsters[i].getX()-player.getX());
        	distY = Math.abs(aggressiveMonsters[i].getY()-player.getY());
        	
        	if (distX<150 && distY<150) {
        		aggressiveMonsters[i].update(this, player, delta);
        	}
        }
        
        // Check if player is near any Items
        for (int i = 0; i < items.length; i++) {
        	distX = Math.abs(items[i].getX()-player.getX());
        	distY = Math.abs(items[i].getY()-player.getY());
        	
        	if (!items[i].isItemCollected() && distX<50 && distY<50) {
        		items[i].specialEffects(player);
        		player.updateInventory(items[i]);
        	}
        }
        
    }
    
    /** Player attacks all monsters in range.
     * 
     * @param delta Time passed since last frame (milliseconds).
     */
    public void combat(double delta) {
    	
    	double distX;
    	double distY;
    	
        // Check if Player near to Passive Monster
        for (int i = 0; i < passiveMonsters.length; i++) {
        	distX = Math.abs(passiveMonsters[i].getX()-player.getX());
        	distY = Math.abs(passiveMonsters[i].getY()-player.getY());
        	
        	if (distX<50 && distY<50) {
        		passiveMonsters[i].isAttacked(delta);
        		player.attack(passiveMonsters[i], delta);
        	}
        }
        
        // Check if Player near to Aggressive Monster
        for (int i = 0; i < aggressiveMonsters.length; i++) {
        	distX = Math.abs(aggressiveMonsters[i].getX()-player.getX());
        	distY = Math.abs(aggressiveMonsters[i].getY()-player.getY());
        	
        	if (distX<50 && distY<50) {
        		player.attack(aggressiveMonsters[i], delta);
        	}
        }
    }    

    /** Render the entire screen, so it reflects the current game state.
     * @param g The Slick graphics object, used for drawing.
     */
    public void render(Graphics g)
    throws SlickException
    {
    	g.setFont(nicefont);
    	
        // Render the relevant section of the map
        int x = -(camera.getMinX() % getTileWidth());
        int y = -(camera.getMinY() % getTileHeight());
        int sx = camera.getMinX() / getTileWidth();
        int sy = camera.getMinY()/ getTileHeight();
        int w = (camera.getMaxX() / getTileWidth()) - (camera.getMinX() / getTileWidth()) + 1;
        int h = (camera.getMaxY() / getTileHeight()) - (camera.getMinY() / getTileHeight()) + 1;
        map.render(x, y, sx, sy, w, h);
        
        // Translate the Graphics object
        g.translate(-camera.getMinX(), -camera.getMinY());
        
        // Render the items
        for (int i = 0; i < items.length; i++)
        	if (!items[i].isItemCollected())
        		items[i].render(items[i].getX(), items[i].getY());
        
        // Render the villagers
        for (int i = 0; i < villagers.length; i++) {
        	villagers[i].render();
        	villagers[i].renderPanel(g);
        }
        
        // Render the monsters
        for (int i = 0; i < passiveMonsters.length; i++) {
        	passiveMonsters[i].render();
        	passiveMonsters[i].renderPanel(g);
        }
        for (int i = 0; i < aggressiveMonsters.length; i++) {
        	aggressiveMonsters[i].render();
        	aggressiveMonsters[i].renderPanel(g);
        }
        
        // Render the player and panel
        player.render();
        player.renderPanel(g);
        
    }

    /** Determines whether a particular map coordinate blocks movement.
     * @param x Map x coordinate (in pixels).
     * @param y Map y coordinate (in pixels).
     * @return true if the coordinate blocks movement.
     */
    public boolean terrainBlocks(double x, double y)
    {
        // Check we are within the bounds of the map
        if (x < 0 || y < 0 || x > getMapWidth() || y > getMapHeight()) {
            return true;
        }
        
        // So the Player doesn't walk into the NPCs
        for (int i = 0; i < villagers.length; i++) {
        	boolean xBlock1 = x > (villagers[i].getX()-villagers[i].getWidth()/2);
        	boolean xBlock2 = x < (villagers[i].getX()+villagers[i].getWidth()/2);
        	boolean yBlock1 = y > (villagers[i].getY()-villagers[i].getHeight()/2);
        	boolean yBlock2 = y < (villagers[i].getY()+villagers[i].getHeight()/2);
        	
        	if (xBlock1 && xBlock2 && yBlock1 && yBlock2)
       			return true;
        }
        
        // Check the tile properties
        int tile_x = (int) x / getTileWidth();
        int tile_y = (int) y / getTileHeight();
        int tileid = map.getTileId(tile_x, tile_y, 0);
        String block = map.getTileProperty(tileid, "block", "0");
        return !block.equals("0");
    }
}
