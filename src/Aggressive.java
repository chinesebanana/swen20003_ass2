/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern Lee <joannal1>
 */

import java.util.Random;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/** Represents the Aggressive Monsters in the game.
 */
public class Aggressive extends Monster {

	// Constants
	private static final double SPEED = 0.25;
	private static final String SOUND_PATH = "/sounds/";
	private static final String SOUND_FILE_TYPE = ".ogg";
	
	// Attack variables
	private Sound attack;
	private double attackTimer;
	private static final int TIMER_START = -1;
	
	/** Creates a new Aggressive Monster.
	 * @param name The name of the Monster
	 * @param imgPath Path of Monster's image file.
     * @param x The Monster's starting x location in pixels.
	 * @param y The Monster's starting y location in pixels.
	 * @param hp The Monster's starting HP value.
     * @param maxDamage The maximum damage the Monster can do.
     * @param cooldown The minimum length of time the Monster has to wait between attacks, in milliseconds.
     * @throws SlickException Library-specific exception.
     */
	public Aggressive(String name, String imgPath, int x, int y, int hp, int maxDamage, int cooldown)
			throws SlickException {
		super(name, imgPath, x, y, hp, maxDamage, cooldown);
		
		attackTimer = TIMER_START;
		attack = new Sound(RPG.ASSETS_PATH + SOUND_PATH + name + SOUND_FILE_TYPE);
	}
	
	/** Get new Y for in regard to Player
	 * @param distTotal Euclid distance between Player and Monster.
	 * @param playerY Y coordinates for Player.
	 * @param delta Time passed since last frame (milliseconds).
	 * @return new y coordinate for Monster.
	 */
	public double getNewY(double distTotal, double playerY, double delta) {
		double distY = ((this.getY() - playerY)/distTotal) * SPEED * delta;
		return getY() - distY;
	}
	
	/** Get new X for in regard to Player 
	 * @param distTotal Euclid distance between Player and Monster.
	 * @param playerX X coordinates for Player.
	 * @param delta Time passed since last frame (milliseconds).
	 * @return new x coordinate for Monster.
	 */
	public double getNewX(double distTotal, double playerX, double delta) {
		double distX = ((this.getX() - playerX)/distTotal) * SPEED * delta;
		return getX() - distX;
	}
	
	/** Moves Monster towards Player
	 * @param world Passed in as an object to check for terrain-blocking 
	 * @param playerX The x coordinates of Player on map.
	 * @param playerY The y coordinates of Player on map.
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void moveTowardsPlayer(World world, double playerX, double playerY, double delta) {
		double distTotal = distance(playerX, playerY);
		double newX, newY;
		
		newX = getNewX(distTotal, playerX, delta);
		newY = getNewY(distTotal, playerY, delta);
		
		if (!world.terrainBlocks(newX, newY)) {
			setX(newX);
			setY(newY);
		}
	}
	
	/** Moves the Monster when within 150p of the Player
	 * @param world Passed in as an object to check for terrain-blocking 
	 * @param player Passed in as an object to check distance and damage HP when attacked
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(World world, Player player, double delta) {
		double playerX = player.getX();
		double playerY = player.getY();
		
		double dist = distance(playerX, playerY);
		
		if (dist >= 50) {
			moveTowardsPlayer(world, playerX, playerY, delta);
		} else if (!isDead() && dist <= 50) {
			if (attackTimer < 0) {
				attack(player);
				attackTimer = getCooldown();
			} else {
				attackTimer -= delta;
			}
		}
	}

    /** Attack the Player with random damage.
     * @param player Passed in as an object to damage HP
     */
	public void attack(Player player) {
    	int damage = new Random().nextInt(getMaxDamage() + 1); // Get damage within range, inclusive
    	attack.play();
    	player.updateHealth(damage);
	}
}
