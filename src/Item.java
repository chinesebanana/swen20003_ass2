/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SlickException;

/** Represents all the items in the game.
 */
public class Item {
	
	private Boolean itemCollected = false;
	private String name;
	private Image img;
	private Sound found;
	
	// The effect the respective items have.
	private static int AMULET_EFFECT = 80;
	private static int SWORD_EFFECT = 30;
	private static int TOME_EFFECT = 300;
	
	// Coordinates on map
	int x, y;
	
	/** Creates a new Item.
	 * @param name The name of the Item.
	 * @param imgPath Path of Item's image file. 
	 * @param x The Item's starting x location in pixels.
	 * @param y The Item's starting y location in pixels.
	 * @throws SlickException Library-specific exception.
	 */
	public Item(String name, String imgPath, int x, int y)
		throws SlickException 
	{
		this.x = x;
		this.y = y;
		this.name = name;
		img = new Image(imgPath);
		found = new Sound(RPG.ASSETS_PATH + "/sounds/found_elixir.ogg");
	}
	
	/** The x coordinate of Item in pixels.
	 * @return x coordinate of Item.
	 */
	public int getX() {
		return x;
	}
	
	/** The y coordinate of Item in pixels.
	 * @return y coordinate of Item.
	 */
	public int getY() {
		return y;
	}
	
	/** Name of the Item 
	 * @return name of Item.
	 */
	public String getName() {
		return name;
	}
	
	/** Checks if Item is collected
	 * @return true if item is collected.
	 */
	public Boolean isItemCollected() {
		return itemCollected;
	}
	
	/** Draw the Item to the screen at the correct place
	 * @param x x coordinates.
	 * @param y y coordinates.
	 */
	public void render(int x, int y) {
		if (!itemCollected)
			img.drawCentered(this.x, this.y);
		else
			img.draw(x, y);
	}
	
	/** Perform Item's special effects on player 
	 * @param player passed in as an object.
	 */
	public void specialEffects(Player player) {
		found.play();
		itemCollected = true;
		
		if (name.equals("amulet"))
			player.updateMaxHp(AMULET_EFFECT);
		else if (name.equals("sword"))
			player.updateMaxDamage(SWORD_EFFECT);
		else if (name.equals("tome"))
			player.updateCooldown(TOME_EFFECT);

	}
}
