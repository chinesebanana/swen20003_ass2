/* 433-294 Object Oriented Software Development
 * RPG Game Engine
 * Sample Solution
 * Author: Matt Giuca <mgiuca>
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Color;
import org.newdawn.slick.Sound;
import java.util.Random;

/** The character which the user plays as.
 */
public class Player extends Unit
{	
	// Player Image
    private Image img_flipped = null;
    private Image which_img = null;
    
    // Panel Image
    private Image panel = null;
    
    // Sound effect when attacking
    private Sound hit;
    private Sound dead;

    // In pixels
    private boolean face_left = false;

    // Pixels per millisecond
    private static final double SPEED = 0.25;
    
    // Starting Stats
    private static final int MAX_HP = 100;
    private static final int MAX_DAMAGE = 26;
    private static final int COOLDOWN = 600;
    private static final int TIMER_START = -1;
    private static final int END_TIMER = 4000;
    private static final int NUM_OF_ITEMS = 4;
    
    // Health and Stats
    private Item[] inventory;
    private int attackTimer;
    
    // To end game
    private int endTimer;
    private boolean end = false; 
    
    /** Creates a new Player.
     * @param image_path Path of player's image file.
     * @param x The Player's starting x location in pixels.
     * @param y The Player's starting y location in pixels.
     * @throws SlickException Library-specific exception.
     */
    public Player(String image_path, double x, double y)
        throws SlickException
    {
        super(image_path, x, y, MAX_HP, MAX_DAMAGE, COOLDOWN);
        
        img_flipped = getImg().getFlippedCopy(true, false);
        panel = new Image(RPG.ASSETS_PATH + "/panel.png");
       
        hit = new Sound(RPG.ASSETS_PATH + "/sounds/player_sword.ogg");
        dead = new Sound(RPG.ASSETS_PATH + "/sounds/player_death.ogg");

        attackTimer = TIMER_START;
        endTimer = END_TIMER;
        
        inventory = new Item[NUM_OF_ITEMS];
        for (int i = 0; i < inventory.length; i++)
        	inventory[i] = null;
    }
    
    /** The health condition the player.
     * @return health percentage.
     */
    public double getHealthPerc()
    {
        return getHp()/getMaxHp();
    }

    /** Gets the current inventory of player
     * @return inventory.
     */
    public Item[] getInventory() {
    	return inventory;
    }

	/** Updates the attackTimer of the Player, counting down to the next time it can attack 
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void updateAttackTimer(double delta) {
		attackTimer -= delta;
	}
	
	/** Updates the attackTimer of the Player, counting down to the next time it can attack 
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void updateEndTimer(double delta) {
		endTimer -= delta;
	}
	
	/** Updates the attackTimer of the Player, counting down to the next time it can attack
	 * @return current timer.
	 */
	public int getEndTimer() {
		return endTimer;
	}
    
    /** Checks if game over
     * @return true if game can end.
     */
    public boolean getEnd() {
    	return end;
    }

    /** Sets end to true when game over */
    public void setEnd() {
    	end = true;
    }

    /** Move the player in a given direction.
     * Prevents the player from moving outside the map space, and also updates
     * the direction the player is facing.
     * @param world The world the player is on (to check blocking).
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     */
    public void move(World world, double dir_x, double dir_y, double delta)
    {
        // Update player facing based on X direction
        if (dir_x > 0)
            this.face_left = false;
        else if (dir_x < 0)
            this.face_left = true;

        // Move the player by dir_x, dir_y, as a multiple of delta * speed
        double new_x = getX() + dir_x * delta * SPEED;
        double new_y = getY() + dir_y * delta * SPEED;

        // Move in x first
        double x_sign = Math.signum(dir_x);
        if(!world.terrainBlocks(new_x + (x_sign * getWidth() / 2), getY() + getHeight() / 2) 
                && !world.terrainBlocks(new_x + (x_sign * getWidth() / 2), getY() - getHeight() / 2)) {
        	setX(new_x);
        }
        
        // Then move in y
        double y_sign = Math.signum(dir_y);
        if(!world.terrainBlocks(getX() + getWidth() / 2, new_y + (y_sign * getHeight() / 2)) 
                && !world.terrainBlocks(getX() - getWidth() / 2, new_y + (y_sign * getHeight() / 2))) {
            setY(new_y);
        }
    }
    
	/** Updates the HP of the Player, checks if the damage is big enough to kill Player 
	 * @param damage The damage sustained by the Player.
	 */
	public void updateHealth(int damage) {
		if (damage >= getHp()) {
			// Player died
			dead.play();
			setX(World.PLAYER_START_X);
			setY(World.PLAYER_START_Y);
			setHp(getMaxHp());
		} else if (damage < 0)
			setHp(getMaxHp());
		else
			setHp(getHp()-damage);
	}

    /** Attack the Monster with random damage.
     * @param monster Passed in as an object to damage HP
     * @param delta Time passed since last frame (milliseconds).
     */
    public void attack(Monster monster, double delta) {
    	if (attackTimer < 0 && !monster.isDead()) {
    		hit.play();
    		int damage = new Random().nextInt(getMaxDamage() + 1); // Get damage within range, inclusive
        	monster.updateHealth(damage);
        	attackTimer = getCooldown();
    	}
    }
    
	/** Updates the Inventory of the Player
	 * @param item The item to be added to the inventory.
	 */
	public void updateInventory(Item item) {
		for (int i = 0; i < inventory.length; i++) {
			if (inventory[i] == null) {
				inventory[i] = item;
				break;
			}
		}
	}
    
    /** Draw the player to the screen at the correct place.
     */
    public void render()
    {
        which_img = this.face_left ? this.img_flipped : this.getImg();
        which_img.drawCentered((int) getX(), (int) getY());
    }

    /** Renders the player's status panel.
     * @param g The current Slick graphics context.
     */
    public void renderPanel(Graphics g)
    {
        // Panel colours
        Color LABEL = new Color(0.9f, 0.9f, 0.4f);          // Gold
        Color VALUE = new Color(1.0f, 1.0f, 1.0f);          // White
        Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f);   // Black, transp
        Color BAR = new Color(0.8f, 0.0f, 0.0f, 0.8f);      // Red, transp

        // Variables for layout
        String text;                // Text to display
        int text_x, text_y;         // Coordinates to draw text
        int bar_x, bar_y;           // Coordinates to draw rectangles
        int bar_width, bar_height;  // Size of rectangle to draw
        int hp_bar_width;           // Size of red (HP) rectangle
        int inv_x, inv_y;           // Coordinates to draw inventory item

        float health_percent;       // Player's health, as a percentage

        int diff_x = (int) getX() - RPG.SCREEN_WIDTH/2;
        int diff_y = (int) getY() - RPG.SCREEN_HEIGHT/2;
        
        // Panel background image
        panel.draw(0 + diff_x, RPG.SCREEN_HEIGHT - RPG.PANEL_HEIGHT + diff_y);

        // Display the player's health
        text_x = 15 + diff_x;
        text_y = RPG.SCREEN_HEIGHT - RPG.PANEL_HEIGHT + 25 + diff_y;
        g.setColor(LABEL);
        g.drawString("Health:", text_x, text_y);
        text = (int) getHp() + "/" + (int) getMaxHp();

        bar_x = 90 + diff_x;
        bar_y = RPG.SCREEN_HEIGHT - RPG.PANEL_HEIGHT + 20 + diff_y;
        bar_width = 90;
        bar_height = 30;
        health_percent = getHp()/getMaxHp();
        hp_bar_width = (int) (bar_width * health_percent);
        text_x = bar_x + (bar_width - g.getFont().getWidth(text)) / 2;
        g.setColor(BAR_BG);
        g.fillRect(bar_x, bar_y, bar_width, bar_height);
        g.setColor(BAR);
        g.fillRect(bar_x, bar_y, hp_bar_width, bar_height);
        g.setColor(VALUE);
        g.drawString(text, text_x, text_y);

        // Display the player's damage and cooldown
        text_x = 200 + diff_x;
        g.setColor(LABEL);
        g.drawString("Damage:", text_x, text_y);
        text_x += 80;
        text = "" + getMaxDamage();
        g.setColor(VALUE);
        g.drawString(text, text_x, text_y);
        text_x += 40;
        g.setColor(LABEL);
        g.drawString("Rate:", text_x, text_y);
        text_x += 55;
        if (attackTimer < 0) 
        	text = "" + getCooldown();
        else 
        	text = "" + attackTimer;
        g.setColor(VALUE);
        g.drawString(text, text_x, text_y);

        // Display the player's inventory
        g.setColor(LABEL);
        g.drawString("Items:", 420 + diff_x, text_y);
        bar_x = 490 + diff_x;
        bar_y = RPG.SCREEN_HEIGHT - RPG.PANEL_HEIGHT + 10 + diff_y;
        bar_width = 288;
        bar_height = bar_height + 20;
        g.setColor(BAR_BG);
        g.fillRect(bar_x, bar_y, bar_width, bar_height);

        inv_x = 490 + diff_x;
        inv_y = RPG.SCREEN_HEIGHT - RPG.PANEL_HEIGHT
            + ((RPG.PANEL_HEIGHT - 72) / 2) + diff_y;
        for (int i = 0; i < inventory.length; i++)
        {
            // Render the item to (inv_x, inv_y)
        	if (inventory[i] != null) {
        		inventory[i].render(inv_x, inv_y);
        		inv_x += 72;
        	}
        }
    }
}
