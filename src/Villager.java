/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Color;

/** Super class for all Villagers
 */
abstract public class Villager extends Unit {
	
	private String dialogue = null;
	
	private static final int MAX_HP = 1;

	private int dialogueTimer;
	private static final int TIME_FOR_DIALOGUE = 3000;
	
	/** Abstract method to return dialogue to be spoken by Villager.
	 * @param player Passed in as an object to check.
	 */
	abstract String dialogue(Player player);
	
    /** Creates a new Villager.
     * @param name The name of the Villager.
     * @param imgPath Path of Villager's image file.
     * @param x The Villager's starting x location in pixels.
     * @param y The Villager's starting y location in pixels.
     * @throws SlickException Library-specific exception.
     */
	Villager(String name, String imgPath, int x, int y)
		throws SlickException
	{
		
		super(name, imgPath, x, y, MAX_HP, 0, 0);
		
		dialogueTimer = -1;
	}
	
	/** Get the dialogue to be said
	 * @return dialogue.
	 */
	public String getDialogue() {
		return dialogue;
	}
	
	/** Set the dialogue to a certain String
	 */
	public void setDialogue(String dialogue) {
		this.dialogue = dialogue;
	}
	
	/** Decrease the dialogue timer by an amount
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void setDialogueTimer(double delta) {
		if (dialogueTimer > 0)
			dialogueTimer -= delta;
	}
	
	/** Start dialogue timer.
	 */
	public void startDialogueTimer() {
		dialogueTimer = TIME_FOR_DIALOGUE;
	}
	
	/** Draw the Villager to the screen at the correct place.
	 */
	public void render() {
		getImg().drawCentered((int) getX(), (int) getY()); 
	}
	
	/** Renders the Villager's health bar.
     * @param g The current Slick graphics context.
     */
	public void renderPanel(Graphics g) {
		
		// Colors used
        Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f);   // Black, transp
        Color BAR = new Color(0.8f, 0.0f, 0.0f, 0.8f);      // Red, transp
        Color NAME = new Color(1.0f, 1.0f, 1.0f);          // White
        
		// Render health bar
        int bar_width = g.getFont().getWidth(getName()) + 20;
        int bar_height = 18;
        int bar_x = (int) getX() - bar_width / 2;
        int bar_y = (int) getY() - 3 * bar_height;
        float health_percent = (getHp()/getMaxHp());
        int hp_bar_width = (int) (bar_width * health_percent);
        g.setColor(BAR_BG);
        g.fillRect(bar_x, bar_y, bar_width, bar_height);
        g.setColor(BAR);
        g.fillRect(bar_x, bar_y, hp_bar_width, bar_height);
        
        // Render name
        int text_x = bar_x + (bar_width - g.getFont().getWidth(getName())) / 2;
        int text_y = bar_y + (bar_height - g.getFont().getHeight(getName())) / 2;
        g.setColor(NAME);
        g.drawString(getName(), text_x, text_y);
        
        // Render dialogue
        if (dialogueTimer > 0 && dialogue != null)
        	renderDialogue(g, dialogue);
	}
	
	/** Renders the Villager's dialogue.
     * @param g The current Slick graphics context.
     * @param dialogue The dialogue to render.
     */
	public void renderDialogue(Graphics g, String dialogue) {
		
		// Colors used
        Color BAR_BG = new Color(0.0f, 0.0f, 0.0f, 0.8f);   // Black, transp
        Color TEXT = new Color(1.0f, 1.0f, 1.0f);          // White
        
		// Render health bar
        int bar_width = g.getFont().getWidth(dialogue) + 20;
        int bar_height = 18;
        int bar_x = (int) getX() - bar_width / 2;
        int bar_y = (int) getY() - 4 * bar_height;
        g.setColor(BAR_BG);
        g.fillRect(bar_x, bar_y, bar_width, bar_height);
        
        // Render name
        int text_x = bar_x + (bar_width - g.getFont().getWidth(dialogue)) / 2;
        int text_y = bar_y + (bar_height - g.getFont().getHeight(dialogue)) / 2;
        g.setColor(TEXT);
        g.drawString(dialogue, text_x, text_y);
        
	}
}
