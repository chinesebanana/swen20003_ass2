/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Joanna Grace Cho Ern LEE <joannal1>
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;


/** Super class for all Units
 */
abstract public class Unit {

	// TODO: FIX JAVA DOC
	// Coordinates on map
	private double x, y;
	private Image img;
	private String name;
	private float hp, maxHp;
    private int maxDamage, cooldown;
	
    // Abstract Methods
	abstract public void render();
    
	/** Creates a new Unit object (Used for Monsters and Villagers)
	 * @param name Name of Unit
     * @param imgPath Path of Unit's image file.
     * @param x The Unit's starting x location in pixels.
     * @param y The Unit's starting y location in pixels.
     * @param hp The Unit's starting HP value.
     * @param maxDamage The maximum damage the Unit can do.
     * @param cooldown The minimum length of time the Unit has to wait between attacks, in milliseconds.
     * @throws SlickException Library-specific exception.
	 */
	public Unit(String name, String imgPath, double x, double y, int hp, int maxDamage, int cooldown)
		throws SlickException
	{
		img = new Image(imgPath);
		
		this.x = x;
        this.y = y;
        maxHp = hp;
        this.hp = maxHp;
        this.maxDamage = maxDamage;
        this.cooldown = cooldown;
        this.name = name;
	}
	
	/** Creates a new Unit object (Used for Player)
     * @param imgPath Path of Unit's image file.
     * @param x The Unit's starting x location in pixels.
     * @param y The Unit's starting y location in pixels.
     * @param hp The Unit's starting HP value.
     * @param maxDamage The maximum damage the Unit can do.
     * @param cooldown The minimum length of time the Unit has to wait between attacks, in milliseconds.
     * @throws SlickException Library-specific exception.
	 */
	public Unit(String imgPath, double x, double y, int hp, int maxDamage, int cooldown)
			throws SlickException
		{
			img = new Image(imgPath);
			
			this.x = x;
	        this.y = y;
	        maxHp = hp;
	        this.hp = maxHp;
	        this.maxDamage = maxDamage;
	        this.cooldown = cooldown;
		}

	/** The x coordinate of Unit in pixels.
	 * @return x coordinate of Unit.
	 */
	public double getX() {
		return x;
	}
	
	/** The y coordinate of Unit in pixels.
	 * @return y coordinate of Unit.
	 */
	public double getY() {
		return y;
	}
	
	/** Set the x coordinate of Unit.
	 * @param new x coordinate of Unit.
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	/** Set the y coordinate of Unit.
	 * @param new y coordinate of Unit.
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	/** Gets the Unit's image.
	 * @return Unit image as an object.
	 */
	public Image getImg() {
		return img;
	}
	
	/** Name of the Unit 
	 * @return name of Unit.
	 */
	public String getName() {
		return name;
	}
	
	/** The width of the Unit's image.
	 * @return width of image.
	 */
	public int getWidth() {
		return img.getWidth();
	}
	
	/** The height of the Unit's image.
	 * @return height of image.
	 */
	public int getHeight() {
		return img.getHeight();
	}
	
	/** Get the current HP of the Unit
	 * @return hp of the Unit.
	 */
	public float getHp() {
		return hp;
	}
	
	/** Set the current HP of the Unit
	 * @param hp new hp of the Unit.
	 */
	public void setHp(float hp) {
		this.hp = hp;
	}
	
	/** Get the Max Damage the Monster can afflict
	 * @return max damage of Monster.
	 */
	public float getMaxHp() {
		return maxHp;
	}
	
	/** Updates the MaxHP of the Player 
	 * @param hp The amount to increase by.
	 */
	public void updateMaxHp(float hp) {
		this.hp += hp;
		maxHp += hp;
	}
	
	/** Get the Max Damage the Monster can afflict
	 * @return max damage of Monster.
	 */
	public int getMaxDamage() {
		return maxDamage;
	}
	
	/** Updates the Max Damage of the Player.
	 * @param damage The amount to increase by.
	 */
	public void updateMaxDamage(int damage) {
		maxDamage += damage;
	}
	
	/** Get the Cooldown time the Monster requires
	 * @return cooldown of Monster.
	 */
	public int getCooldown() {
		return cooldown;
	}
	
	/** Updates the Cooldown of the Player.
	 * @param cooldown The amount to decrease by.
	 */
	public void updateCooldown(int cooldown) {
		this.cooldown -= cooldown;
	}
	
}
